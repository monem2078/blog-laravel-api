<?php

namespace App\Http\Controllers\Api\Auth\Traits;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

trait VerifyUsers
{
    /**
     * Check if user is verified.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|null|\Symfony\Component\HttpFoundation\Response
     */
    public function accountVerified(Request $request)
    {
        $user = $request->user();

        if ($user->is_active) {
            return null;
        }

        return $this->generateToken($request);
    }

    /**
     * Get the username field in the database.
     *
     * @return string
     */
    protected function getUsername()
    {
        return 'mobile';
    }

    /**
     * To generate login token.
     *
     * @param Request $request
     * @return UserResource
     */
    public function generateToken(Request $request)
    {
        /** @var User $user */
        $user = $request->user();

        $user->addOneSignalToken($request->input('onesignal-player-id'));

        $tokenResult = $user->createToken(
            $request->input('device', 'Unkown device'),
            ['*']
        );

        return (new UserResource($user))->additional([
            'token' => $tokenResult->accessToken,
        ]);
    }
}
