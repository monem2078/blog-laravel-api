<?php

namespace App\Http\Controllers\Api\Auth;

use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\UserResource;
use App\Http\Controllers\Api\Controller as BaseController;

class LoginController extends BaseController
{
    /**
     * Login to the application, returns token.
     *
     * @param Request $request
     * @return UserResource|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        // Check if the user exists.
        if ($errorResponse = $this->attemptUser($request)) {
            return $errorResponse;
        }

        return $this->generateToken($request);
    }

    /**
     * check user email and password.
     *
     * @param Request $request
     * @return null|\Symfony\Component\HttpFoundation\Response
     */
    public function attemptUser(Request $request)
    {
        $this->validateRequest($request);
        $credentials = $this->credentials($request);

        // Check user's main information
        if (! Auth::attempt($credentials)) {
            return response([
                'errors' => [
                    $this->username() => [trans('auth.failed')],
                ],
            ], Response::HTTP_UNAUTHORIZED);
        }

        return null;
    }

    /**
     * To generate login token.
     *
     * @param Request $request
     * @return UserResource
     */
    public function generateToken(Request $request)
    {
        /** @var User $user */
        $user = $request->user();

        $tokenResult = $user->createToken(
            $request->input('device', 'Unkown device'),
            ['*']
        );

        return (new UserResource($user))->additional([
            'token' => $tokenResult->accessToken,
        ]);
    }

    /**
     * Get the credentials main user information for the login request.
     *
     * @param $request
     *
     * @return array
     */
    private function credentials($request)
    {
        return [
            $this->username() => $request->input($this->username()),
            'password' => $request->input('password'),
        ];
    }

    /**
     * Get the username field in the database.
     *
     * @return string
     */
    protected function username()
    {
        return 'email';
    }

    /**
     * Validate the login request.
     *
     * @param $request
     */
    private function validateRequest($request)
    {
        $this->validate($request, [
            $this->username() => 'required',
            'password' => 'required',
        ]);
    }
}
