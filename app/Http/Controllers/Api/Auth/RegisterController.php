<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Http\Controllers\Api\Controller as BaseController;

class RegisterController extends BaseController
{
    /**
     * Register a new teacher.
     *
     * @param Request $request
     * @return string
     */
    public function Register(Request $request)
    {
        $user = new User(array_merge($request->all(), [
            'password' => bcrypt($request->password),
        ]));

        $user->save();

        // Return success message with hateoas
        return $this->registered($request, $user);
    }

    /**
     * Returns success message with hateoas.
     *
     * @param User $user
     * @return UserResource
     */
    private function registered(Request $request, User $user)
    {
        $token = $this->generateTokenFor($request, $user);

        return (new UserResource($user))->additional([
            'message' => trans('auth.register'),
            'token' => $token,
        ]);

        return (new UserResource($user));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return string
     */
    private function generateTokenFor(Request $request, User $user): string
    {
        return $user->createToken(
            $request->input('device', 'Unkown device'),
            ['*']
        )->accessToken;
    }
}
