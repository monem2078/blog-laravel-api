<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Support\ResetPassword;
use App\Http\Controllers\Api\Controller as BaseController;

class ForgotPasswordController extends BaseController
{
    /**
     * @var ResetPassword
     */
    private $resetPassword;

    /**
     * ForgotPasswordController constructor.
     * @param ResetPassword $resetPassword
     */
    public function __construct(ResetPassword $resetPassword)
    {
        $this->resetPassword = $resetPassword;
    }

    /**
     * Send code to mobile.
     *
     * @param Request $request
     * @return null|array
     */
    public function sendCode(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
        ]);

        $code = $this->resetPassword->createVerificationCode($request->email);

        // check if email verified
        if (! $code) {
            return response([
                'errors' => [
                    'code' => [trans('auth.not_active')],
                ],
            ], Response::HTTP_UNAUTHORIZED);
        }

        // TODO: Send message

        return [
            'check-the-code' => route('api.auth.password.verifyCode'),
        ];
    }

    /**
     * Check the code.
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function verifyCode(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'code' => 'required|min:6|max:6',
        ]);

        $token = $this->resetPassword->checkCode($request->email, $request->code);

        // No token has returned
        if (! $token) {
            return response([
                'errors' => [
                    'code' => [trans('passwords.reset.wrong-code')],
                ],
            ], Response::HTTP_UNAUTHORIZED);
        }

        return [
            'token' => $token,
        ];
    }
}
