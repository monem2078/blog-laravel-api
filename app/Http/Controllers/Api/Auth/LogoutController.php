<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\ServiceToken;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class LogoutController extends Controller
{
    /**
     * Destroy the token for the user and the onesignal token.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function logout(Request $request)
    {
        $onesignalToken = $request->input('onesignal-player-id');

        ServiceToken::onesignal()->where('token', $onesignalToken)->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
