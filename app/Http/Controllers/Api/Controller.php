<?php

namespace App\Http\Controllers\Api;

use Response;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Number of items per page.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * Number of messages per page.
     *
     * @var int
     */
    protected $messagesPerPage = 200;

    /**
     * {@inheritdoc}
     */
    protected function formatValidationErrors(Validator $validator)
    {
        return ['errors' => $validator->errors()->getMessages()];
    }

    /**
     * Check if the client wants pagination.
     *
     * @return bool
     */
    protected function wantsPagination()
    {
        return request()->exists('page');
    }

    /**
     * @param $errors
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function respondWithValiationErrors($errors)
    {
        return response(compact('errors'), Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
